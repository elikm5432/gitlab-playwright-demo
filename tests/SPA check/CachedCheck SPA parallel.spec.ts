import test from "@playwright/test";
import flights from "./Destinations SPA.json";
import axios from "axios";

const webhookUrl =
  "https://hooks.slack.com/services/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";

function convertToNumber(currencyString) {
  return parseFloat(currencyString.replace(/[^\d.-]/g, ""));
}

function isSignificantDifference(value1, value2) {
  const isRubles = value1.includes("₽");
  const number1 = convertToNumber(value1);
  const number2 = convertToNumber(value2);
  const difference = Math.abs(number1 - number2);
  return isRubles ? difference > 100 : difference > 5;
}

async function sendSlackMessage(message) {
  const slackMessage = {
    text: message,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: message,
        },
      },
    ],
  };
  await axios.post(webhookUrl, slackMessage);
}
let rubCard = 1;
async function checkFlights(page, flight, reverse) {
        for (let card = 0; card < rubCard; card++) {
        await chooseDestination(page, flight);
    if (reverse) {
      await page
        .locator('button[aria-label="İstiqaməti dəyişdirin"]')
        .click();
    }
    await selectCard(page, card);
    await page.locator('.monthsContainer_jfI').waitFor({ state: "visible" });
      const enabledDates = page.locator(
        'button[class*="day_"]:not([disabled])'
      );
      const firstMonth = page
        .locator('button[class*="monthListButton_"]')
        .first();
      await page
        .locator('button[data-qa="calendar_one_way_button"]')
        .waitFor({ state: "visible" });
      const enabledDatesCount = await enabledDates.count();
      for (let selectedDate = 0; selectedDate < enabledDatesCount; selectedDate++) {
        if (selectedDate > 0) {
          const outboundDate = page.locator("button[class*=outboundDate]");
          await outboundDate.waitFor({ state: "visible" });
          await outboundDate.click();
        }
        if (await firstMonth.isEnabled()) {
          await firstMonth.click();
        }
        await enabledDates.nth(selectedDate).click();
        const oneWayButton = page.locator(
          'button[data-qa="calendar_one_way_button"]'
        );
        if (await oneWayButton.isVisible()) {
          oneWayButton.click();
        }
        const search_button = page.locator('button[data-qa="search_button"]');
        await search_button.dblclick();
        const flightOptions = page.locator(".root_LBm");
        await flightOptions.nth(0).waitFor({ state: "visible" });
        const okButton = page.getByRole("button", {
          name: "OK",
          exact: true,
        });

        if (await okButton.nth(0).isVisible()) {
          await okButton.nth(0).click();
          continue;
        }
        console.log(reverse ? `SPA check ${flight.origin} - ${flight.destination} day ${selectedDate+1}/${enabledDatesCount}`: `SPA check ${flight.destination} - ${flight.origin} day ${selectedDate+1}/${enabledDatesCount}`);
        const flightCount = await flightOptions.count();
        for (let flightNumber = 0; flightNumber < flightCount; flightNumber++) {
          const planeChip = await page
            .locator('.root_LBm .planeChip_HrV')
            .nth(flightNumber)
            .getAttribute("title");

          if (!planeChip?.includes('S7'||'TK')) {
            continue;
          }

          const ekonomButtons = page.locator(
            '.root_PF6 button.tariffButton_bSF:has(span.tariffType_mfL:text("Ekonom"))'
          );

          const biznesButtons = page.locator(
            '.root_PF6 button.tariffButton_bSF:has(span.tariffType_mfL:text("Biznes"))'
          );

          if (await ekonomButtons.nth(flightNumber).isEnabled()) {
            await ekonomButtons.nth(flightNumber).click();
            const chooseTariffButton = page.locator(
              "button[class*=chooseTariffButton]"
            );
            await chooseTariffButton.nth(0).waitFor({ state: "visible" });
            const priceKlassikLocator = page.locator(
              'article:has(h3:has-text("Proreyt ekonom")) .price_VI3'
            );
            const priceKlassik = await priceKlassikLocator.textContent();
            const klassikChooseTariffButtonLocator = page.locator(
              'article:has-text("Proreyt ekonom") .chooseTariffButton_qkK'
            );
            const klassikChooseTariffButton =
              await klassikChooseTariffButtonLocator.first();
            await klassikChooseTariffButton.click();

            const flightDetailsButton = page.locator(
              'button[class*="flight_details_toggle"]'
            );
            await flightDetailsButton.waitFor({ state: "visible" });
            const planeChip = await page
                .locator('div[class*="planeChip_"]')
                .nth(0)
                .getAttribute("title");
              const dateElement = page
                .locator(
                  "//div[contains(@class, 'locationAndDate_E0u')]/span[3]"
                )
                .first();
              let dateText = await dateElement.textContent();
            await flightDetailsButton.click();
            const totalAmountLocator = page.locator(
              "section.tariff_o1b div.tariff_row_ST9 span:last-child"
            );
            const tariffTypeLocatorFirst = page
              .locator("div.flight_row_tWc span:last-child")
              .first();
            let tariffTypeFirst = await tariffTypeLocatorFirst.textContent();
            const tariffTypeLocatorLast = page
              .locator("div.flight_row_tWc span:last-child")
              .last();
            let tariffTypeLast = await tariffTypeLocatorLast.textContent();

            if (
              (await totalAmountLocator.textContent()) != priceKlassik &&
              isSignificantDifference(
                await totalAmountLocator.textContent(),
                priceKlassik
              )
            ) {
              await sendSlackMessage( reverse?
                `*Price mismatch Klassik:* ${planeChip} ${flight.origin} - ${
                  flight.destination
                } ${dateText} ${await totalAmountLocator.textContent()} !== ${priceKlassik}`:
                `*Price mismatch Klassik:* ${planeChip} ${flight.destination} - ${
                  flight.origin
                } ${dateText} ${await totalAmountLocator.textContent()} !== ${priceKlassik}`
              );
            }
            await page.locator('button[class*="closeButton"]').click();
             await page.locator('button[class*="editButton"]').click();
          }
          
          const biznesButton = biznesButtons.nth(flightNumber);

          if (await biznesButton.isEnabled()) {
            await biznesButton.click();
            const chooseTariffButton = page.locator(
              "button[class*=chooseTariffButton]"
            );            
            let prices = page.locator('[class^="price_VI3"]');
            let priceBiznes = await prices.nth(0).textContent();
            await chooseTariffButton.nth(0).waitFor({ state: "visible" });
            await chooseTariffButton.nth(0).click();
            const flightDetailsButton = page.locator(
              'button[class*="flight_details_toggle"]'
            );
            await flightDetailsButton.waitFor({ state: "visible" });
            await flightDetailsButton.click();

            const totalAmountLocator = page.locator(
              "section.tariff_o1b div.tariff_row_ST9 span:last-child"
            );
            let totalAmount = await totalAmountLocator.textContent();
            const planeChip = await page
              .locator('div[class*="planeChip_"]')
              .nth(0)
              .getAttribute("title");
            const dateElement = page
              .locator(
                "//div[contains(@class, 'locationAndDate_E0u')]/span[3]"
              )
              .first();
            let dateText = await dateElement.textContent();

            if (
              (await totalAmountLocator.textContent()) != priceBiznes &&
              isSignificantDifference(
                await totalAmountLocator.textContent(),
                priceBiznes
              )
            ) {
              await sendSlackMessage( reverse?
                `*Price mismatch Biznes:* ${planeChip} ${flight.origin} - ${
                  flight.destination
                } ${dateText} ${await totalAmountLocator.textContent()} !== ${priceBiznes}`:
                `*Price mismatch Biznes:* ${planeChip} ${flight.destination} - ${
                  flight.origin
                } ${dateText} ${await totalAmountLocator.textContent()} !== ${priceBiznes}`
              );
            }
            await page.locator('button[class*="closeButton"]').click();
            await page.locator('button[class*="editButton"]').click();
          }
        }
      }
    }
  console.log(reverse ? `SPA check ${flight.origin} - ${flight.destination} finished`: `SPA check ${flight.destination} - ${flight.origin} finished`);
}
test.beforeAll(async ({}) => {
  const slackMessage = {
    text: `*SPA check* started at ${new Date().toLocaleString()}`,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*SPA check* started at ${new Date().toLocaleString()}`,
        },
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*Destinations list:*\n${JSON.stringify(
            flights.flights,
            null,
            2
          )}`,
        },
      },
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `*Test description:*\nCached SPA prices check on 2 tariffs: Klassik Biznes`,
        },
      },
    ],
  };
  await axios.post(webhookUrl, slackMessage);
});
for (let flight of Object.values(flights.flights)) {
  for(let reverse = 0; reverse < 2; reverse++){
    const testName = reverse ? `SPA check - ${flight.destination} - ${flight.origin}`: `SPA check - ${flight.origin} - ${flight.destination}`;
    
test(testName, async ({ page }) => {
    await checkFlights(page, flight, reverse);
});
}}
async function selectCard(page: any, card: number) {
  if (await page.locator('div[class*="_selects"]').isVisible()) {
    rubCard = 2;
  } else { rubCard = 1; }
  if (card) {
    await page.locator("div[class*=css-b62m3t-container]").click();
    await page.getByText('Beynəlxalq kart', { exact: true }).click();
    if (await page.locator("button[class*=_withoutDatesBlock_]").isVisible()) {
      await page.locator("button[class*=_withoutDatesBlock_]").click();
    }
  }
}

async function chooseDestination(page: any, flight: any) {
  await page.goto("https://book.azal.az/");
  await page.locator('label:has-text("BAK")').waitFor({ state: "visible" });
  await page.getByLabel("Haradan").fill(flight.origin);
  await page.locator("ul > button:first-of-type").click();
  await page.getByLabel("Haraya").fill(flight.destination);
  await page.locator("ul > button:first-of-type").click();
}

